#!/usr/bin/sbcl --script

(defun make-board (width height)
 "Builds a game board that is `width` by `height`"
 (make-array (list width height) :element-type 'symbol :initial-element nil))

(defun pretty-print-board (board)
 "Pretty print the game board"
 (loop for y below (array-dimension board 0) do
      (loop for x below (array-dimension board 1) do
	   (let ((place (aref board y x)))
	    (format t "~a "
		    (ecase place
		      ((nil) ".")
		      (red "R")
		      (black "B")))))
      (terpri)))

(defun column-in-bounds-p (board col)
 (< -1 col (array-dimension board 1)))

(defun move-valid-p (board col)
 "Checks if the column `col` exists and has space on `board`"
 (and (column-in-bounds-p board col) (null (aref board 0 col))))

(defun make-move (board col color)
 "Places a `color` piece in the `col` column of `board`"
 (loop for i from (1- (array-dimension board 0)) downto 0 do
      (when (null (aref board i col))
       (setf (aref board i col) color)
       (return))))

(defun check-winner-helper (board x y color dir depth)
 "Helper function for check-winner. Does all the real work."
 (let ((board-width (array-dimension board 1)))
  ;; Check for initial space being empty
  (if (null (aref board y x))
      (return-from check-winner-helper))
  ;; See if we have enough depth
  (if (= depth 4)
      ;; Check for color match
      (if (eq (aref board y x) color)
	  (return-from check-winner-helper color)
	  (return-from check-winner-helper)))
  ;; Check for color match
  (if (not (eq (aref board y x) color))
      (return-from check-winner-helper))
  ;; Check for hitting left edge
  (if (and (eq dir 'nw) (= x 0))
      (return-from check-winner-helper))
  ;; Check for hitting right edge
  (if (and (member dir '(ne e)) (= x (1- board-width)))
      (return-from check-winner-helper))
  ;; Check for hitting top
  (if (and (member dir '(nw n ne)) (= y 0))
      (return-from check-winner-helper))
  ;; So far so good - continue checking the line
  (ecase dir
    (nw (check-winner-helper board (1- x) (1- y) color 'nw (1+ depth)))
    (n (check-winner-helper board x (1- y) color 'n (1+ depth)))
    (ne (check-winner-helper board (1+ x) (1- y) color 'ne (1+ depth)))
    (e (check-winner-helper board (1+ x) y color 'e (1+ depth))))))

(defun check-winner (board)
 "Check for a winner on the game board."
 (let ((board-width (array-dimension board 1))
       (board-height (array-dimension board 0)))
  (loop for y from (1- board-height) downto 0 do
       (loop for x below board-width do
	    (loop for dir in '(nw n ne e) do
		 (loop for color in '(red black) do
		      (let ((res (check-winner-helper board x y color dir 1)))
		       (if (member res '(black red))
			   (return-from check-winner res)))))))
  ;; No winners - check if moves are available
  (loop for x below board-width do
       (if (null (aref board 0 x))
	   (return-from check-winner)))
  ;; No moves available; it's a tie
  'tie))

(defun get-int (&optional (prompt ""))
 "Get column choice input for game loop."
 (let ((number nil))
  (loop until (numberp number) do
       (format t prompt)
       (finish-output)
       (setf number (parse-integer (read-line) :junk-allowed t)))
  number))

(defun player-human (board color)
 "Human player routine - allow them to chose column to play in"
 (let ((choice (1- (get-int "~%Column to place: "))))
  (loop until (move-valid-p board choice) do
       (format t "~%Sorry, not a valid move. Try again.~%")
       (setf choice (1- (get-int "~%Column to place: "))))
  choice))

(defun player-random-cpu (board color)
 "CPU player routine - pick a random valid move"
 (terpri)
 (loop for rand = (random (array-dimension board 1)) do
      (if (move-valid-p board rand) (return rand))))

(defun register-player-functions ()
 "Get all functions starting with 'player-', for user presentation"
 (let (funs)
  (loop for sym being the symbols in 'cl-user do
       (let ((str (symbol-name sym)))
	(if (and (fboundp sym)
		 (>= (length str) (length "player-"))
		 (string-equal str "player-" :end1 (length "player-")))
	    (push sym funs))))
  funs))

(defun game-loop (turn-functions)
 "Main loop. `turn-functions` is a hash table with symbols RED and BLACK as functions"
 (let ((board (make-board 8 8)))
  (loop
     (loop for player in '(red black) do
	  ;; Display board and turn info
	  (pretty-print-board board)
	  (format t "~%~a's turn~%" (string-capitalize (symbol-name player)))
	  ;; Handle move
	  (make-move board
		     (funcall (gethash player turn-functions) board player)
		     player)
	  ;; Check for wins
	  (let ((winner (check-winner board)))
	   (when winner
	    (pretty-print-board board)
	    (ecase winner
	      (red (format t "~%Red wins!~%"))
	      (black (format t "~%Black wins!~%"))
	      (tie (format t "~%Tie! No one wins!~%")))
	    (return-from game-loop)))))))

(defun choose-players ()
 (let ((players (register-player-functions))
       (functions (make-hash-table)))
  (loop for color in '(red black) do
       (loop
	  for i from 1 to (length players)
	  for a in players do
	    (format t "~d - ~a~%" i a))
       (let ((choice -1))
	(loop until (< -1 (1- choice) (length players)) do
	     (setf choice (get-int
			   (format nil "Player for ~a: "
				   (string-capitalize (symbol-name color))))))
	(setf (gethash color functions) (nth (1- choice) players))))
  functions))

(defun main ()
 (format t "~%Welcome to Connect4L!~%")
 (setf *random-state* (make-random-state t))
 (game-loop (choose-players)))

(main)
